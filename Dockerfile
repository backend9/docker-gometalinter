FROM    golang:1.14-alpine3.11

RUN     apk --update upgrade && \
        apk add make curl git build-base wget openssh bash && \
        rm -rf /var/cache/apk/*

RUN     eval $(ssh-agent -s)    &&\
        mkdir -p ~/.ssh         &&\
        echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config &&\
        git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"

RUN     GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint@v1.23.6

